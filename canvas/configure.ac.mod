
# This is just to make configure fail in case the module-openprs-client.pc is not found.
# Otherwise the following variables are empty and this is unnoticed until you try to load the module.
PKG_CHECK_MODULES($module$, $module$-openprs-client)

$MODULE$_OPENPRS_MODDIR=`$PKG_CONFIG '$module$-openprs-client' --variable=oprsmoddir`
AC_SUBST($MODULE$_OPENPRS_MODDIR)

$MODULE$_OPENPRS_DATADIR=`$PKG_CONFIG '$module$-openprs-client' --variable=oprsdatadir`
AC_SUBST($MODULE$_OPENPRS_DATADIR)
