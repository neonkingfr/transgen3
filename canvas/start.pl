#----------------------------------------------------------------------
# Debut du script de generation des sources d'un superviseur transgen
# Heavily inspired from Genom.
# $Id$
#----------------------------------------------------------------------


# add newline on print
$\ = "\n";

# Definition de $MAKE
# (utilisation de gnumake car problemes de compatibilite de make avec gnumake)
my $MAKE = "gnumake";

# move_if_change
sub move_if_change {
    local ($a, $b) = @_;
    if ( -f $b ) {
        if (system("cmp -s $a $b") == 0) {
            unlink $a;
        } else {
            print "$b changed";
            system("mv $a $b");
        }
    } else {
        print "$b created";
        system("mv $a $b");
    }
}

sub mirror_dir {
  my ($srcdir, $dstdir, $include, $exclude, $confirm) = @_;

  if (! -d "$dstdir") {
    my $agree='y';
    if ($confirm) {
      printf ("create $dstdir (y/n)? ");
      chomp($agree=<STDIN>);
    }
    if ($agree eq 'y') {
      mkdir "$dstdir", 04775;
    } else { return; }
  }

  opendir(DIR, $srcdir);
  foreach(readdir(DIR)) {
    # skip unwanted files
    if ($exclude ne "") { if (/$exclude/) { next; } }
    if ($include ne "") { if (!/$include/) { next; } }
    if (! -f "$srcdir/$_") { next; }

    if (! -f "$dstdir/$_") {
      # file doesn't exist
      system("cp $srcdir/$_ $dstdir/$_");

    } elsif (system("cmp -s $srcdir/$_ $dstdir/$_") != 0) {
      # file exists and is different
      if ($confirm) {
	printf ("overwrite $dstdir/$_ (y/n)? ");
	my $agree;
	chomp($agree=<STDIN>);
	if ($agree eq 'y') {
	  printf ("save $dstdir/$_ in $dstdir/$_.transgensave\n");
	  system ("cp $dstdir/$_ $dstdir/$_.transgensave");
	} else {
	  printf ("keeping $dstdir/$_\n");
	  next;
	}
      }
      system ("'cp' -f $srcdir/$_ $dstdir/$_");
    }
  }
  closedir(DIR);
}

# Creation du repertoire auto
#
if (! -d "auto") {
    print "directory auto created";
    mkdir "auto", 04775;
}
else {
    chmod 04775, 'auto';
}

# Creation du repertoire autoconf
#
if (! -d "autoconf") {
    print "directory autoconf created";
    mkdir "autoconf", 04775;
}
else {
    chmod 04775, 'autoconf';
}

#---------------------------------------------------------------
