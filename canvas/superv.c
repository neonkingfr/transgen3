/*
 * This file is generated automatically, do not edit by hand.
 */

/*                               -*- Mode: C -*- 
 * $superv-name$-superv.c -- 
 * 
 * Copyright (C) 1999-2012 LAAS/CNRS
 * 
 * Author          : Francois Felix Ingrand
 * Created On      : Thu Jul 15 17:10:37 1999
 * Last Modified By: Francois Felix Ingrand
 * Last Modified On: Wed Sep 22 14:06:54 1999
 * Update Count    : 6
 * Status          : OK
 * 
 * $Id$
 * 
 */


/* Inclusions generales */
#  include <stdio.h>
#  include <portLib.h>

#include <sys/types.h>

#include <hashPack.h>

#include "macro-pub.h"


#include "csLib.h"

#include "opaque-pub.h"
#include "oprs-type-pub.h"
#include "constant-pub.h"

#include "pu-enum_f.h"
#include "pu-mk-term_f.h"
#include "pu-parse-tl_f.h"

#include "oprs-type_f-pub.h"

#include "user-decl-oprs-post.h"
#include "user-decl-oprs-com.h"
#include "user-decl-oprs-post_f.h"
#include "user-decl-oprs-com_f.h"

/* #include "macro.h" */


$include-modules$


void init_module_table()
{
$declare-modules$
}

void init_global_enum()
{
     PU_DECLARE_ENUM_ATOM(OK);
}
