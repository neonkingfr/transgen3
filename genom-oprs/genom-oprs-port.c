/*                               -*- Mode: C -*-
 *
 * Copyright (c) 1994-2013 Francois Felix Ingrand, LAAS/CNRS.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    - Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    - Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>

#include <hashPack.h>

#include "macro.h"

#include "opaque-pub.h"
#include "macro-pub.h"
#undef FREE

#include "oprs-type-pub.h"
#include "pu-mk-term_f.h"
#include "pu-parse-tl_f.h"

#include "oprs-type_f-pub.h"
#include "lisp-list_f-pub.h"


#include "genom3/c/client.h"

#include "genom-oprs.h"
#include "genom-oprs_f.h"
#include "genom-oprs-service.h"
#include "genom-oprs-service_f.h"
#include "genom-oprs-port_f.h"

typedef struct {
     Symbol name;
     const struct genom_port_info *genom_port_info;
     Decode_Func_Proto decode_func;
} Port;


#define PORT_HASH_SIZE 16		/* has to be a power of two */
static int port_hashtable_size = PORT_HASH_SIZE;
static int port_hashtable_mod = PORT_HASH_SIZE -1;

static int port_hash_a_string(PString name )	
{
     return (hash_a_string(name) & port_hashtable_mod);
}

static PBoolean port_match(Symbol f1, Port *port)
{
     return ( f1 == port->name) ;
}

HASH make_port_hashtable() {
     return make_hashtable(port_hashtable_size,
			   port_hash_a_string,
			   port_match);
}

void declare_genom3_port(char *name, Mod *mod,
		    const struct genom_port_info *genom_port_info, 
		    Decode_Func_Proto decode_func)
{
     Port *port = MAKE_OBJECT(Port);
     
     port->name = declare_atom(name);	/* Declare the port as a symbol */
     port->decode_func = decode_func;
     port->genom_port_info = genom_port_info;

     if ((get_from_hashtable(mod->port_hash, port->name)) != NULL)
	  fprintf(stderr, "genom-oprs: The port \"%s\" has already been declared.\n", port->name); 
     
     add_to_hashtable(mod->port_hash, port, port->name); 

     return;
}   


Term *read_port_internal(Symbol mod_name, Symbol port_name, Symbol name)
{
     Port *port;
     Term *res;
     Mod *mod;

     if ((mod = (Mod *)get_from_hashtable(go_mod_hash,
					  mod_name)) == NULL) {
	  fprintf(stderr, "genom-oprs:read_port: could not find module: %s.\n", mod_name); 
	  return build_nil();
     } else if ((port = (Port *)get_from_hashtable(mod->port_hash,
						port_name)) == NULL) {
	  fprintf(stderr, "genom-oprs:read_port: could not find port: %s.\n", port_name); 
	  return build_nil();
     } else if (!mod->initialized && !init_genom_oprs_module(mod)) {
	  fprintf(stderr, "genom-oprs:read_port: module %s not initialized.\n", mod_name);
	  return build_nil();
     } else {
	  void *data;
	  const struct genom_port_info *gpi;
	  
	  gpi = port->genom_port_info;
	  
	  data = malloc(gpi->data_size);
	  gpi->init_data(data);
	  if (gpi->simple) {
	       if (name) 
		    fprintf(stderr, "genom-oprs:read_port: %s seems to be a regular port, but you are passing another name argument: %s.\n", port_name, name); 
	       gpi->read.simple(mod->genom_client, data);
	  } else {
	       if (!name) {
		    fprintf(stderr, "genom-oprs:read_port: %s seems to be a  port, but you forgot the extra argument.\n", port_name);
		    return build_nil();
	       } else {
		    gpi->read.multiple(mod->genom_client, name, data);
	       }
	  }
	  res = (*(port->decode_func))(data);
	  gpi->fini_data(data);
	  free(data);
	  return res;
     }
}
     
Term *read_port_name(TermList terms)
/* This is the action which read the port. */
{
     char *port_name;
     char *name;
     char *mod_name;

     if (! (PUGetOprsParameters(terms, 3, ATOM, &mod_name, ATOM, &port_name, ATOM, &name))) {
	  fprintf(stderr, "genom-oprs:read_port_name: unable to parse read_port_name arguments.\n"); 
	  return  build_nil();
     }
     
     return read_port_internal(mod_name, port_name, name);
}

Term *read_port(TermList terms)
/* This is the action which read the port. */
{
     char *port_name;
     char *mod_name;

     if (! (PUGetOprsParameters(terms, 2, ATOM, &mod_name, ATOM, &port_name))) {
	  fprintf(stderr, "genom-oprs:read_port: unable to parse read_port argument.\n"); 
	  return  build_nil();
     }
     return read_port_internal(mod_name, port_name, NULL);
}

