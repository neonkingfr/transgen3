/*                               -*- Mode: C -*- 
 * oprs-post_f.h -- 
 * 
 * Copyright (C) 1997-2012 Francois Felix Ingrand
 * 
 * Author          : Francois Felix Ingrand
 * Created On      : Fri Nov 28 15:40:04 1997
 * Last Modified By: Francois Felix Ingrand
 * Last Modified On: Thu Jul  1 17:29:27 1999
 * Update Count    : 8
 * Status          : OK
 * 
 * $Id$
 */


extern Term *read_port_name(TermList terms);
extern Term *read_port ( TermList terms );
extern Term *read_port_l_list ( TermList terms );
//Term *ef_list_ports(TermList terms);

