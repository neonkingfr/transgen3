/*                               -*- Mode: C -*-
 *
 * Copyright (c) 1994-2017 Francois Felix Ingrand, LAAS/CNRS.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    - Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    - Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <time.h>
#include <sys/time.h>


#include <listPack.h>
#include <hashPack.h>


#include "macro.h"

#include "macro-pub.h"

#include "constant-pub.h"
#include "opaque-pub.h"
#include "macro-pub.h"
#include "oprs-type-pub.h"
#include "pu-mk-term_f.h"
#include "pu-parse-tl_f.h"

#include "oprs-type_f-pub.h"
#include "lisp-list_f-pub.h"
#include "fact-goal_f-pub.h"
#include "oprs_f-pub.h"
#include "top-lev_f-pub.h"

#include "genom3/c/client.h"

#include "socket_server.h"
#include "genom-oprs.h"
#include "genom-oprs_f.h"
#include "genom-oprs-service.h"
#include "genom-oprs-service_f.h"

#define COPY_LIST(list) copy_list((list),NOT_NODES)
#define FREE_LIST(list) free_list((list),NOT_NODES)

typedef struct {		/* Request type */
     Symbol name;		/* Name (a symbol) */
     const struct genom_service_info *genom_service_info; /* we have got everything in there...  */
     Encode_Func_Proto encode_func; /* Encode and decode functions */
     Decode_Func_Proto decode_func;
} Service;

typedef struct {		/* An Active Request */
     int rqid;			/* its id (mine) */
     Service *service;		/* its service */
     Mod *mod;			/* Ist module */
     int ext_rqid;		/* its id */
     PBoolean ir_received;	/* has it received an intermediate reply yet */
} Rqst_Instance;


/* Various hash tables */

#define MOD_HASH_SIZE 16		/* has to be a power of two */
static int mod_hashtable_size = MOD_HASH_SIZE;
static int mod_hashtable_mod = MOD_HASH_SIZE - 1;
HASH go_mod_hash;

#define SERVICE_HASH_SIZE 128		/* has to be a power of two */
static int service_hashtable_size = SERVICE_HASH_SIZE;
static int service_hashtable_mod = SERVICE_HASH_SIZE -1;

static PBoolean genom_oprs_verbose = TRUE;


void end_genom_oprs ( void );
void init_genom_oprs(Symbol name);

static void post_fr_report(Symbol from, Rqst_Instance *rqst_instance, Term *term, Term *report);


/* Hash functions. */
static int mod_hash_a_string(PString name)
{
     return (hash_a_string(name) & mod_hashtable_mod);
}
static PBoolean mod_match(PString s1, Mod *mod)
{
     return (s1 == mod->name) ;	/* These are atoms, so they can be pointer compared */
}


static int service_hash_a_string(PString name )
{
     return (hash_a_string(name) & service_hashtable_mod);
}
static PBoolean service_match(PString s1, Service *service)
{
     return (s1 == service->name) ;	/* These are atoms, so they can be pointer compared */
}


/* The global reply message queue or client id. */
//CLIENT_ID rsId;

static void end_com_to_g3_module(OPRS_NODE ignore, Mod *mod)
{
     if (mod->genom_client) {		/* if valid genom_client, shutdown it. */
	  remove_external_readfds(mod->genom_client_info->eventfd(mod->genom_client));
	  mod->genom_client_info->fini(mod->genom_client);
     }
     /* Should we clean up and free everything? */
}

void end_genom_oprs()
{
     /* terminate the logging socket server */
     end_server();

     for_all_hashtable(go_mod_hash, NULL, (SL_PFI)end_com_to_g3_module); /* for all known modules */
}

static Pred_Func_Rec *ir_pred, *fr_pred;

void init_genom_oprs(Symbol name)
{
     /* Init the logging socket server */
     start_server(3140);

     ir_pred = declare_pred_from_symbol(declare_atom("IR"));
     fr_pred = declare_pred_from_symbol(declare_atom("FR"));

     go_mod_hash = make_hashtable(mod_hashtable_size, mod_hash_a_string, mod_match);
}

Mod *declare_genom3_module(char * name, const struct genom_client_info *genom_client_info, 
			   Term *(*genom_oprs_mod_exception)(genom_event e, const void *detail))
{
     Mod *mod = MAKE_OBJECT(Mod);

     mod->name = declare_atom(name); /* the name of this particular instance */
     mod->genom_client = NULL;
     mod->genom_client_info = genom_client_info;

     mod->genom_oprs_mod_exception = genom_oprs_mod_exception;
     mod->generic_name = declare_atom((char *)genom_client_info->name); /* the generic name of this module */

     mod->rqst_instance_list = make_list();
     mod->initialized = FALSE;
     mod->port_hash = make_port_hashtable();

     mod->service_hash = make_hashtable(service_hashtable_size, 
					  service_hash_a_string, 
					  service_match);

     if ((get_from_hashtable(go_mod_hash,mod->name)) != NULL)
	  fprintf(stderr, "genom-oprs:redeclaring the module:%s.\n", mod->name);

     add_to_hashtable(go_mod_hash, mod, mod->name); /* redeclaring it anyway... */

     return mod;
}

PBoolean init_genom_oprs_module(Mod *mod)
{
     int argc = 2;
     char *argv[3];

     argv[0] = strdup("-i");	/* strdup not needed */
     argv[1] = strdup(mod->name); /* strdup not needed */
     argv[2] = NULL;		/*  this is not really needed a priori */
 
     genom_event ex;
     const void *exdetail;

     if (!mod->initialized) {
	  if ((mod->genom_client = mod->genom_client_info->init(argc,argv,&ex,&exdetail)) == NULL) {
	       Term *exception = (*(mod->genom_oprs_mod_exception))(ex, exdetail);

	       post_fr_report(mod->name, NULL, PUMakeTermNil(), exception);

	       fprintf(stderr, "genom-oprs:init_genom_oprs_module:client_init_func for %s returned NULL, %s, %p.\n", mod->name, ex, exdetail);
	  } else {
	       mod->initialized = TRUE;
	       add_external_readfds(mod->genom_client_info->eventfd(mod->genom_client));
//	       mod->name = declare_atom(*(mod->genom_client_info->instance)(mod->genom_client));
	  }
     } else
	  fprintf(stderr, "genom-oprs:init_genom_oprs_module: module: %s, already initialized.\n", mod->name);

//     fprintf(stderr, "argv[0] %s , argv[1] %s\n",argv[0], argv[1]);
     
     return mod->initialized;
}

static PBoolean init_genom_oprs_name_com(Symbol mod_name)
{
     Mod *mod;

     if ((mod = (Mod *)get_from_hashtable(go_mod_hash,mod_name)) == NULL) {
	  fprintf(stderr, "genom-oprs:init_genom_oprs_name_com:undeclared module: %s.\n", mod_name);
	  return FALSE;
     } else
	  return init_genom_oprs_module(mod);
}

Term *init_com_to_g3_module(TermList terms)
{
     Term *t1;

     t1 = (Term *)get_list_pos(terms,1);
     if (t1->type != ATOM) {
	  fprintf(stderr,"Expecting an ATOM in init_com_to_g3_module.\n");
	  return build_nil();
     } else {
	  if (init_genom_oprs_name_com(t1->u.id))
	       return build_t();
	  else
	       return build_nil();
     }
}

void declare_genom3_service(char *name, Mod *mod, const struct genom_service_info *genom_service_info, 
//genom_request_sendfn genom_request_sendfn, 
		    Encode_Func_Proto encode,// int encode_size,
		    Decode_Func_Proto decode //, int decode_size)
     )
{
     Service *service = MAKE_OBJECT(Service);

     service->name = declare_atom(name);	/* Declare the service as a symbol */
     service->genom_service_info = genom_service_info;
     service->encode_func = encode;
     service->decode_func = decode;

     if ((get_from_hashtable(mod->service_hash, service->name)) != NULL)
	  fprintf(stderr, "genom-oprs: WARNING: redeclaring the service:%s for module %s.\n", service->name, mod->name);

     add_to_hashtable(mod->service_hash, service, service->name);
}

/* Function to post the intermediate reply fact. */
static void post_ir(Symbol from, Rqst_Instance *rqst_instance, int ir_letter)
{
     TermList terms;

     terms = make_list();
     add_to_tail(terms, build_id(from));
     add_to_tail(terms, build_id(rqst_instance->service->name));
     add_to_tail(terms, build_integer(rqst_instance->rqid));
     add_to_tail(terms, build_integer(ir_letter));
     add_fact(make_fact_from_expr(build_expr_pfr_terms(ir_pred,
						       terms))
	      ,current_oprs);
}

/* Function to post the final reply fact. */

static void post_fr_report(Symbol from, Rqst_Instance *rqst_instance, Term *term, Term *report)
{
     TermList terms2;

     terms2 = make_list();
     add_to_tail(terms2, build_id(from));
     add_to_tail(terms2, build_id((rqst_instance?rqst_instance->service->name:"Initialisation")));
     add_to_tail(terms2, build_integer(rqst_instance?rqst_instance->rqid:-1));
     add_to_tail(terms2, report);
     add_to_tail(terms2, term);

     add_fact(make_fact_from_expr(build_expr_pfr_terms(fr_pred,
						       terms2)),
	      current_oprs);
}

void genom_oprs_service_cb(genom_client h, int g3_rqid, int done, genom_event report, void *output, const void *exdetail, void *cb_data)
{			
     Rqst_Instance *rqst_instance = (Rqst_Instance *)cb_data;
     Mod *mod = rqst_instance->mod; 
     Term *output_term;
     PBoolean del_rqst = FALSE;

     if (genom_oprs_verbose) {
	  fprintf(stderr, "genom-oprs: --- genom_service_cb: getting reply  %s, from %s, rqid %d, ext_rqid %d, g3_rqid %d, done %s, report %s.\n",
		  rqst_instance->service->name, mod->name, rqst_instance->rqid, 
		  rqst_instance->ext_rqid, g3_rqid, (done?"FR":"IR"), ((report == genom_ok)?"OK":"not OK"));
     }
     
     if (report == genom_ok) {
	  if (!done){  /* IR */
	       if (!rqst_instance->ir_received) {
		    post_ir(rqst_instance->mod->name, rqst_instance, g3_rqid);
		    rqst_instance->ir_received = TRUE;
	       }
	  } else {		/* FR tout est OK */
	       if (rqst_instance->service->genom_service_info->output_size) { /* if size null, nothing to decode */
		    output_term = (*(rqst_instance->service->decode_func))(output);
// pas a moi de le faire rqst_instance->service->genom_service_info->fini_output(output);

	       } else {
		    output_term = PUMakeTermNil();
	       }
	       
	       post_fr_report(rqst_instance->mod->name, rqst_instance, output_term, PUMakeTermAtom("OK"));
	       del_rqst = TRUE;
	       if (genom_oprs_verbose) {
		    printf("genom-oprs: <-- receive %s from %s, rqid %d\n", rqst_instance->service->name, rqst_instance->mod->name, rqst_instance->rqid);
		    fprintf(stderr,"genom-oprs: <-- receive %s from %s, rqid %d\n", rqst_instance->service->name, rqst_instance->mod->name, rqst_instance->rqid);
	       }
	       server_send("[FR %d \"OK\"]", rqst_instance->rqid);
	  }

     } else {		/* report not OK */

	  Term *exception = (*(mod->genom_oprs_mod_exception))(report, exdetail);

	  output_term = PUMakeTermNil();
	  post_fr_report(rqst_instance->mod->name, rqst_instance, output_term, exception);
	  del_rqst = TRUE;
     }
     
     if (del_rqst) {
	  mod->genom_client_info->clean(h, g3_rqid);
	  delete_list_node(rqst_instance->mod->rqst_instance_list,rqst_instance); 
	  /* clean de la requete */
	  FREE_OBJECT(rqst_instance);
     }

     return;
}

static void check_module(OPRS_NODE ignore, Mod *mod)
{
     if (mod->initialized) {	/* We need to check this as we can arrive here before mod->genom_client has a value. */
	  if (! sl_slist_empty(mod->rqst_instance_list)) { /* if this module has no pending rqst, skip it */
//	  fprintf(stderr, "genom-oprs:check_module\n");
	       genom_event e;

	       e = mod->genom_client_info->doevents(mod->genom_client); /* this will call the callback for all new events. */
	       if (e != genom_ok) {
		    fprintf(stderr,"genom-oprs:check_module: did not get genom_ok (%s) while checking module %s.\n", e, mod->name);
	       }
	  }
     }
}

PBoolean genom_oprs_loaded_module(TermList terms)
{
  char *mod_name;
   
  if (! (PUGetOprsParameters(terms, 1, ATOM, &mod_name))) {
    fprintf(stderr, "genom-oprs:genom_oprs_loaded_module:GetOprsParameters:unable to parse request.\n");
    return FALSE;
  } else {
    return (get_from_hashtable(go_mod_hash, mod_name) != NULL);
  }
}

PBoolean genom_oprs_get_reply_and_check_com(TermList ignore)
{
     Rqst_Instance *rqst_instance;

     static int tick_count = 0;
     struct timeval real_time;
     struct timezone tzp;
     
     if (gettimeofday(&real_time,&tzp) != 0)
	  perror("gettimeofday");
     else
          server_send("[TICK %d %ld%06ld]", tick_count, real_time.tv_sec, real_time.tv_usec);

     ++tick_count;

     for_all_hashtable(go_mod_hash, NULL, (SL_PFI)check_module); /* for all known modules */

     return FALSE;		/* Never return TRUE, unless we want to stop. */
}

Term *send_request_to_g3_module(TermList terms)
{
     static int rqid = 0;
     Service *service;
     Mod *mod;
     int encode_res, status;
     genom_event ires;
     Term * res;
     char *mod_name;
     char *service_name;

     rqid++;

     if (! (PUGetOprsParameters(terms, 2, ATOM, &mod_name, ATOM, &service_name))) {
	  fprintf(stderr, "genom-oprs:send_request_to_g3_module:GetOprsParameters:unable to parse request.\n");
	  res = build_integer(-1);
     } else if ((mod = (Mod *)get_from_hashtable(go_mod_hash, mod_name)) == NULL) {
	  fprintf(stderr, "genom-oprs:send_request_to_g3_module:could not find module %s.\n", mod_name);
	  res = build_integer(-1);
     } else if ((service = (Service *)get_from_hashtable(mod->service_hash, service_name)) == NULL) {
	  fprintf(stderr, "genom-oprs:send_request_to_g3_module:could not find request service %s.\n", service_name);
	  res = build_integer(-1);
     } else if (!mod->initialized && !init_genom_oprs_module(mod)) {
	  fprintf(stderr, "genom-oprs:send_request_to_g3_module: module %s not initialized.\n", mod_name);
	  res = build_integer(-1);
     } else {
	  Rqst_Instance *rqst_instance;
	  char *rq_letter;
	  Expression *term;
	  
	  if (service->genom_service_info->input_size) { /* if size is null, then nothing to encore (even if the function "exists")... */
	       
	       if (! PUGetOprsParametersSpecArg(terms, 3, EXPRESSION, &term)) {
		    fprintf(stderr, "genom-oprs:send_request_to_g3_module:PUGetOprsParametersSpecArg:unable to parse request.\n");
		    return build_integer(-1);
	       }

	       rq_letter = (char *)malloc(service->genom_service_info->input_size);
	       service->genom_service_info->init_input(rq_letter);

	       encode_res = (*(service->encode_func))(
		    term,		/* Term */
		    (void *)(rq_letter)); /* Structure pointer */

	       if (! encode_res) {
		    fprintf(stderr, "genom-oprs:send_request_to_g3_module:problem while encoding data\n\
\tfor request %s for module %s.\n", service_name, mod_name);
		    free(rq_letter);
		    return build_integer(-1);
	       }
	  }

	  rqst_instance = MAKE_OBJECT(Rqst_Instance);
	  rqst_instance->rqid = rqid;
	  rqst_instance->service = service;
	  rqst_instance->mod = mod;
	  rqst_instance->ir_received = FALSE;

	  ires = service->genom_service_info->send(mod->genom_client, rq_letter, genom_oprs_service_cb, genom_oprs_service_cb, rqst_instance,  &(rqst_instance->ext_rqid));
	  
	  if (ires == genom_ok) {

	       service->genom_service_info->fini_input(rq_letter);

	       server_send("[RQ %s %d]", service_name, rqid);
	       if (genom_oprs_verbose) {
		    fprintf(stderr,"genom-oprs: --> send request %s to %s, rqid %d\n", service_name, mod_name, rqid);
		    printf("genom-oprs: --> send request %s to %s, rqid %d\n", service_name, mod_name, rqid);
	       }
	       add_to_tail(mod->rqst_instance_list, rqst_instance);
	  } else {
	       genom_context ctxt =  mod->genom_client_info->context(mod->genom_client);
	       
	       Term *exception = (*(mod->genom_oprs_mod_exception))(ires,ctxt->raised(NULL, ctxt));

	       post_fr_report(mod_name, rqst_instance, PUMakeTermNil(), exception);

	       fprintf(stderr,"genom-oprs:send_request_to_g3_module: did not get genom_ok while sending\n\
\trequest %s to module %s.\n", service_name, mod_name);
	       return build_integer(-1);
	  }

	  res = build_integer(rqid);
     }
     return res;
}

Term *set_genom_oprs_verbose(TermList terms)
/* This is the action which sends the request. */
{
     Term *t1;

     t1 = (Term *)get_list_pos(terms,1);
     if (t1->type != ATOM) {
	  fprintf(stderr,"Expecting an ATOM in set_genom_oprs_verbose.\n");
	  return build_nil();
     } else {
	  genom_oprs_verbose = ! (t1->u.id == nil_sym);
	  return build_t();
     }
}

