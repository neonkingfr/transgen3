/*                               -*- Mode: C -*- 
 * oprs-com_f.h -- 
 * 
 * Copyright (C) 1998-2017 LAAS/CNRS
 * 
 * Author          : Francois Felix Ingrand
 * Created On      : Fri Jun  5 17:01:17 1998
 * Last Modified By: Francois Felix Ingrand
 * Last Modified On: Thu Jul  1 17:27:22 1999
 * Update Count    : 11
 * Status          : OK
 * 
 * $Id$
 */

HASH make_port_hashtable(void);
PBoolean init_genom_oprs_module(Mod *mod);
Term *init_com_to_g3_module ( TermList terms );
//Term *init_com_force_to_module(TermList terms);
Term *send_request_to_g3_module ( TermList terms );
Term *set_genom_oprs_verbose(TermList terms);
//Term *ef_strerror(TermList terms);
PBoolean genom_oprs_get_reply_and_check_com(TermList ignore);
PBoolean genom_oprs_loaded_module(TermList terms);
//int abort_encode(void *data, TermList terms);
//Term *ef_list_modules(TermList terms);
//Term *ef_list_initialized_modules(TermList terms);
//Term *ef_list_requests(TermList terms);
void end_genom_oprs(void);
void init_genom_oprs(Symbol name);
