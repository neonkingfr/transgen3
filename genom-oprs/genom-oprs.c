/*                               -*- Mode: C -*- 
 * genom-oprs startup....
 * 
 * Copyright (C) 1999-2017 LAAS/CNRS
 * 
 * Author          : Francois Felix Ingrand
 * Created On      : Mon Jul 19 15:01:12 1999
 * Last Modified By: Francois Felix Ingrand
 * Last Modified On: Wed Sep 10 12:20:12 2003
 * Update Count    : 11
 * Status          : OK
 * 
 * $Id$
 */

#include <stdio.h>

#include "listPack.h"

#include "macro-pub.h"
#include "opaque-pub.h"
#include "constant-pub.h"
#include "oprs-type-pub.h"

#include "pu-mk-term_f.h"
#include "pu-parse-tl_f.h"
#include "pu-enum_f.h"

#include "oprs_f-pub.h"
#include "oprs-type_f-pub.h"
#include "user-end-hook_f-pub.h"


#include "default-hook.h"
#include "default-hook_f.h"
#include "user-action.h"
#include "user-ev-function.h"
#include "user-ev-predicate.h"
#include "int-graph_f-pub.h"
#include "intention_f-pub.h"
#include "action_f-pub.h"
#include "ev-predicate_f-pub.h"

#include "genom3/c/client.h"


#include "genom-oprs.h"
#include "genom-oprs_f.h"
#include "genom-oprs-service.h"
#include "genom-oprs-service_f.h"

#include "genom-oprs-port_f.h"

void declare_genom_oprs_action(void)
{
     make_and_declare_action("SEND-REQUEST-TO-G3-MODULE",send_request_to_g3_module, 3);
     make_and_declare_action("INIT-COM-TO-G3-MODULE",init_com_to_g3_module, 1);
     make_and_declare_action("SET-GENOM-OPRS-VERBOSE",set_genom_oprs_verbose, 1);

     make_and_declare_action("READ-PORT",read_port, 2);
     make_and_declare_action("READ-PORT-NAME",read_port_name, 3);

     return;
}

void declare_genom_oprs_eval_funct(void)
{
     /* make_and_declare_eval_funct("LIST-MODULES", ef_list_modules, 0); */
     /* make_and_declare_eval_funct("LIST-INITIALIZED-MODULES", ef_list_initialized_modules, 0); */
     /* make_and_declare_eval_funct("LIST-REQUESTS", ef_list_requests, 0); */
     /* make_and_declare_eval_funct("LIST-PORTS", ef_list_ports, 0); */

//     PU_enum_declare_user_eval_funct();

     return;
}

void declare_genom_oprs_eval_pred(void)
{
     make_and_declare_eval_pred("G3O-GET-REPLY-CHECK-COM",
				genom_oprs_get_reply_and_check_com,
				0, TRUE);
     make_and_declare_eval_pred("G3O-LOADED-MODULE",
				genom_oprs_loaded_module,
				1, TRUE);
     return;
}

Intention_List transgen_par_intention_scheduler_with_priority(Intention_List l)
{
     Intention_List l2 = make_list();
     Intention *intention;
     PBoolean first = TRUE;
     int prio = 0;		/* Initialized to please gcc... */

     l = (Intention_List)sort_list_func(l,&intention_list_sort_by_priority);
     loop_through_list(l, intention, Intention *)
	  if (first) {
	       prio = intention_priority(intention);
	       add_to_head(l2, intention);
	       first = FALSE;
	  } else
	       if (prio == intention_priority(intention))
		    add_to_head(l2, intention);
	       else
		    break;
     oprs_free_list(l);

     return l2;
}

Intention_List transgen_intention_scheduler_time_sharing_with_sort_predicate(Intention_List l)
{
     if (intention_list_sort_predicate)
	  l = (Intention_List)sort_list_func(l,intention_list_sort_predicate);
     return l;
}

void end_kernel_user_hook()
{
     fprintf(stderr,"Shutting down genom-oprs...\n");
     end_genom_oprs();
     fprintf(stderr,"OpenPRS GenoM3 Supervision shutting down...\n");

}

void init_transgen_genom_oprs(void)
{
     declare_genom_oprs_action();
     declare_genom_oprs_eval_funct();
     declare_genom_oprs_eval_pred();

     PU_init_atom_enum_hash();
     init_genom_oprs(kernel_name());

     add_user_end_kernel_hook((PFV)end_kernel_user_hook);
}

