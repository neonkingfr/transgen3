
extern Mod *declare_genom3_module(char * name, const struct genom_client_info *genom_client_info, 
				  Term *(*genom_oprs_mod_error)(genom_event e, const void *detail));

extern void declare_genom3_service(char *name, Mod *mod, const struct genom_service_info *genom_service_info, 
			   Encode_Func_Proto encode,
			   Decode_Func_Proto decode);

extern void declare_genom3_port(char *name, Mod *mod,
				const struct genom_port_info *genom_port_info, 
				Decode_Func_Proto decode_func);
