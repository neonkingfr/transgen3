/*                               -*- Mode: C -*- 
 * macro.h -- 
 * 
 * Copyright (C) 1997 LAAS/CNRS
 * 
 * Author          : Francois Felix Ingrand
 * Created On      : Wed Apr 20 16:34:31 1994
 * Last Modified By: Francois Felix Ingrand
 * Last Modified On: Fri Nov 28 15:44:43 1997
 * Update Count    : 3
 * Status          : OK
 * 
 * $Id$
 */

#ifndef	MIN
#define	MIN(a,b) (((a)<(b))?(a):(b))
#endif

#define NEW_STRING(from, to) do {int len = strlen(from);\
				   to = (char *)malloc(len+1);\
				   memcpy(to,from,len+1);\
				   } while (0)

