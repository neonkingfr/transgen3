/* -*- c -*-
 * $Id: memory.c,v 1.1 2004/06/18 11:41:58 fpy Exp $
 *
 * Author(s):
 *  - Fr�d�ric Py <fpy@laas.fr>
 *
 * Creation Date: Jun 02, 2004
 */
#include <stdio.h>
#include <sys/time.h>

#define DEBUG
#include "memory.h"
#undef DEBUG

static FILE *dot_out = NULL;
static unsigned long n_alloc = 0, n_free = 0, mem_used = 0;

void dot_add() {
  if( dot_out!=NULL ) {
    struct timeval time;
    
    gettimeofday(&time, NULL);
    fprintf(dot_out, "%lu.%.6lu\t%lu\n", time.tv_sec, time.tv_usec, mem_used);
  }
}

void *dbg_malloc(size_t obj_size) {
  size_t *result;
  
  result = malloc(obj_size+sizeof(size_t));
  if( result!=NULL ) {
    *(result++) = obj_size;
    mem_used += obj_size;
    ++n_alloc;
    dot_add();
  }
  return result;
}

void *dbg_calloc(size_t n_elem, size_t obj_size) {
  return dbg_malloc(n_elem*obj_size);
}

void dbg_free(void *to_del) {
  if( to_del!=NULL ) {
    size_t *tmp = to_del, freed;

    freed = *(--tmp);
    --n_alloc;
    ++n_free;
    mem_used -= freed;
    dot_add();
    free(tmp);
  }
}

void dbg_finish() {
  if( dot_out!=NULL ) {
    fclose(dot_out);
    dot_out = NULL;
  }
}

void dbg_trace(char *file_name) {
  if( dot_out==NULL ) {
    dot_out = fopen(file_name, "w");
    if( dot_out!=NULL ) {
      dot_add();
      atexit(dbg_finish);
    }
  }
}

