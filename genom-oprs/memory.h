/* -*- c -*-
 * $Id: memory.h,v 1.2 2004/08/25 14:25:01 fpy Exp $
 *
 * Author(s):
 *  - Fr�d�ric Py <fpy@laas.fr>
 *
 * Creation Date: May 24, 2004
 */
#ifndef R2C_MEMORY_HEADER
#define R2C_MEMORY_HEADER

#include <stdlib.h>
/* #include "macro-pub.h" */

#ifdef DEBUG

#define R2C_MALLOC dbg_malloc
#define R2C_CALLOC dbg_calloc
#define R2C_FREE   dbg_free

void *dbg_malloc(size_t obj_size);
void *dbg_calloc(size_t n_elem, size_t obj_size);
void dbg_free(void *to_del);

void dbg_trace(char *file_name);
#define MEM_TRACE(name) dbg_trace(name)

#else /* !DEBUG */

#define R2C_MALLOC malloc
#define R2C_CALLOC calloc
#define R2C_FREE   free

#define MEM_TRACE(name) ((void)0)

#endif /* DEBUG */

#define OBJ_CREATE(type) (type *)R2C_MALLOC(sizeof(type))
#define TAB_CREATE(type, size) (type *)R2C_CALLOC(size, sizeof(type))

typedef void (*free_fn)(void *);
typedef void *(*cpy_fn)(void *);

#define NO_DEL ((free_fn)NULL)
#define NO_CPY ((cpy_fn)NULL)

#endif /* !R2C_MEMORY_HEADER */
