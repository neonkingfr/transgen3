/* -*- c -*-
 * $Id: simple_list.c,v 1.3 2004/07/02 13:44:14 fpy Exp $
 *
 * Author(s):
 *  - Fr�d�ric Py <fpy@laas.fr>
 *
 * Creation Date: Jun 02, 2004
 */
#include "simple_list.h"

simple_list sl_create() {
  return NULL;
}

void sl_destroy(simple_list this, free_fn del_node) {
  simple_list_iterator iter;
  
  if( del_node!=NO_DEL )
    for( iter = sl_begin(this); !sl_end_p(iter); ) 
      del_node(sl_pop_head(iter));
  else
    for( iter = sl_begin(this); !sl_end_p(iter); ) 
      sl_pop_head(iter);
}

simple_list sl_dup(simple_list this, cpy_fn cpy) {
  simple_list ret = sl_create();
  simple_list_iterator i_me = sl_begin(this), i_ret = sl_begin(ret);
  void *cur;
  
  if( cpy==NO_CPY ) { 
    sl_for_each(i_me, void *, cur) {
      sl_add_head(cur, i_ret);
      sl_advance(&i_ret);
    }
  } else {
    sl_for_each(i_me, void *, cur) {
      sl_add_head(cpy(cur), i_ret);
      sl_advance(&i_ret);
    }
  }
  return ret;
}

int sl_add_head(void *elem, simple_list_iterator this) {
  simple_list tmp = OBJ_CREATE(struct simple_list_cell);
  
  if( tmp==NULL )
    return -1;
  else {
    tmp->remaining = *this;
    tmp->content = elem;
    *this = tmp;
    return 0;
  }
}

void *sl_pop_head(simple_list_iterator this) {
  if( sl_end_p(this) )
    return NULL;
  else {
    simple_list tmp = *this;
    void *ret_val = car_simple_list(tmp, void *);

    *this = cdr_simple_list(tmp);
    R2C_FREE(tmp);
    return ret_val;
  }
}

int sl_empty_p(simple_list l) {
  return l==NULL;
}

int sl_advance(simple_list_iterator *iter) {
  if( iter==NULL )
    return -1;
  else if( !sl_end_p(*iter) ) {
    *iter = &cdr_simple_list(**iter);
    return 0;
  } else
    return 1;
}

simple_list_iterator sl_find(void *elem, cmp_fn cmp, 
			     simple_list_iterator this) {
  void *cur;

  sl_for_each(this, void *, cur)
    if( cmp(elem, cur)>=0 )
      break;
  return this;
}

size_t sl_size(simple_list l) {
  size_t res=0;
  void *toto;
  simple_list_iterator iter = sl_begin(l);
  sl_for_each(iter, void *, toto)
    ++res;
  return res;
}
