/* -*- c -*-
 * $Id: simple_list.h,v 1.3 2004/07/02 13:44:14 fpy Exp $
 *
 * Author(s):
 *  - Frédéric Py <fpy@laas.fr>
 *
 * Creation Date: Jun 02, 2004
 */
#ifndef R2C_SIMPLE_LIST_HEADER
#define R2C_SIMPLE_LIST_HEADER

#include "simple_list_t.h"
#include "memory.h"

simple_list sl_create();
void sl_destroy(simple_list this, free_fn del_node);

simple_list sl_dup(simple_list this, cpy_fn cpy);

int sl_add_head(void *elem, simple_list_iterator this);
void *sl_pop_head(simple_list_iterator this);

int sl_empty_p(simple_list l);
size_t sl_size(simple_list l);

simple_list_iterator sl_find(void *elem, cmp_fn cmp, simple_list_iterator this);

#define sl_begin(l) (&(l))
#define sl_end_p(iter) sl_empty_p(*(iter))

int sl_advance(simple_list_iterator *iter);

#define sl_for_each(iter, type, name) \
  for(; !sl_end_p(iter) &&\
      ( (name=car_simple_list(*(iter), type)) || 1 ); sl_advance(&(iter)) )

#endif /* !R2C_SIMPLE_LIST_HEADER */
