/* -*- c -*-
 * $Id: simple_list_t.h,v 1.1 2004/06/18 11:41:58 fpy Exp $
 *
 * Author(s):
 *  - Frédéric Py <fpy@laas.fr>
 *
 * Creation Date: Jun 02, 2004
 */
#ifndef R2C_SIMPLE_LIST_T_HEADER
#define R2C_SIMPLE_LIST_T_HEADER

typedef struct simple_list_cell *simple_list;
typedef simple_list *simple_list_iterator;

struct simple_list_cell {
  void        *content;
  simple_list remaining;
}; /* struct simple_list_cell */

#define car_simple_list(l, type) ((type)((l)->content))
#define cdr_simple_list(l) ((l)->remaining)

typedef int (*cmp_fn)(void *, void *);

#endif /* !R2C_SIMPLE_LIST_T_HEADER */
