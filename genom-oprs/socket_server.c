/* -*- c -*-
 * $Id: socket_server.c,v 1.12 2005/03/22 16:47:21 fpy Exp $
 *
 * Author(s):
 *  - Frédéric Py <fpy@laas.fr>
 *
 * Creation Date: Aug 26, 2004
 */

#include <signal.h>

#include <stdio.h>
#include <stdarg.h>
#include <string.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include <sys/time.h>
#include <unistd.h>
#include <fcntl.h>

#include "socket_server.h"
#include "memory.h"
#include "simple_list.h"
#include "string_base.h"

#ifndef HOST_NAME_MAX
# define HOST_NAME_MAX 255
#endif /* HOST_NAME_MAX */

int my_sock = 0;
char *my_host_name = NULL;

void close_server() {
  if( NULL!=my_host_name ) {
    if( 0!=my_sock ) {
      fprintf(stdout, "R2C: Closing socket server\n");
      shutdown(my_sock, SHUT_RDWR);
      my_sock = 0;
      fprintf(stdout, "R2C: socket closed\n");
    }
    R2C_FREE(my_host_name);
    my_host_name = NULL;
  }
}

int init_server(int port) {
  int ret;
  size_t len;
  struct sockaddr_in sa;

  if( NULL==my_host_name ) {
    my_host_name = TAB_CREATE(char, HOST_NAME_MAX+1);
    my_host_name[HOST_NAME_MAX] = '\0';

    if( (ret = gethostname(my_host_name, HOST_NAME_MAX))<0 ) {
      fprintf(stderr, "init_server:gethostname: error.\n");
      close_server();
      return ret;
    }
    if( (ret = socket(AF_INET, SOCK_STREAM, 0))<0 ) {
      perror("init_server:socket");
      close_server();
      return ret;
    }
    my_sock = ret;

    fcntl(my_sock, F_SETFL, O_NONBLOCK);

    sa.sin_family = AF_INET;
    sa.sin_addr.s_addr = htonl(INADDR_ANY);
    sa.sin_port = htons((unsigned short)port);
    len = sizeof(sa);

    if( (ret = bind(my_sock, (struct sockaddr *)&sa, len))<0 ) {
      perror("init_server:bind");
      close_server();
      return ret;
    }
    listen(my_sock, 5);
  }
  return 0;
}

void start_server(int port) {
  if( my_sock==0 ) {
    init_server(port);
    signal(SIGPIPE, SIG_IGN);
  }
}

void end_server() {
  close_server();
}

#define MAX_MSG_LEN 1024
static simple_list my_imsgs = NULL;
static int client = 0;


void socket_send() {
  if( client==0 ) {
    struct sockaddr_in sa;
    socklen_t len;
    int ns;

    len = sizeof(sa);
    ns = accept(my_sock, (struct sockaddr *)&sa, &len);
    if( ns>0 )
      client = ns;
  }
  while( client>0 && !sl_empty_p(my_imsgs) ) {
    size_t msg_len, net_size;
    char *msg = (char *)sl_pop_head(sl_begin(my_imsgs));

    msg_len = strlen(msg);
    net_size = htonl(msg_len);
    if( 0>send(client, &net_size, sizeof(net_size), 0) ) {
      close(client);
      client = 0;
    } else if( 0>send(client, msg, msg_len, 0) ) {
      close(client);
      client = 0;
    }
    if( 0!=client )
      R2C_FREE(msg);
    else
      sl_add_head(msg, sl_begin(my_imsgs));
  }
}

void server_send(char *format, ...) {
  char buf[MAX_MSG_LEN+1], *msg;
  simple_list_iterator iter;
  va_list ap;

  buf[MAX_MSG_LEN] = '\0';
  va_start(ap, format);
  vsnprintf(buf, MAX_MSG_LEN, format, ap);
  va_end(ap);

  msg = string_dup(buf);
  for(iter = sl_begin(my_imsgs); !sl_end_p(iter); sl_advance(&iter));
  //  fprintf(stderr, "R2C : sending message \"%s\"\n", msg);
  sl_add_head(msg, iter);

  /* now I try to send messages */
  if( my_sock>0 )
    socket_send();
}

