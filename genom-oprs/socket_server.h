/* -*- c -*-
 * $Id: socket_server.h,v 1.1 2004/08/27 10:01:18 fpy Exp $
 *
 * Author(s):
 *  - Fr�d�ric Py <fpy@laas.fr>
 *
 * Creation Date: Aug 26, 2004
 */
#ifndef R2C_SOCKET_SERVER_HEADER
#  define R2C_SOCKET_SERVER_HEADER

void start_server(int port);
void end_server();

void server_send(char *format, ...);

#endif // R2C_SOCKET_SERVER_HEADER
 
