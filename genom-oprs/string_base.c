/* -*- c -*-
 * $Id: string_base.c,v 1.3 2004/07/05 14:02:14 fpy Exp $
 *
 * Author(s):
 *  - Fr�d�ric Py <fpy@laas.fr>
 *
 * Creation Date: Jun 05, 2004
 */
#include <stddef.h>

#include "string_base.h"


char *sub_string(char const *from, char const *to) {
  char *res = NULL, *iter;
  if( from!=NULL ) {
    if( to==NULL || '\0'==*to )
      return string_dup(from);
    else {
      ptrdiff_t la = to-from;

      if( la>0 ) {
	res = TAB_CREATE(char, la+1);
	
	for(iter=res; from!=to ; ++from, ++iter)
	  if( '\0'==(*iter=*from) ) {
	    iter = string_dup(res);
	    R2C_FREE(res);
	    return iter;
	  } 
	*iter = '\0';
      }
    }
  }
  return res;  
}

char *string_dup(char const *str) {
  return string_cat(str, NULL);
}

char *string_cat(char const *a, char const *b) {
  size_t la = string_length(a), lb = string_length(b);
  
  if( 0==la && 0==lb )
    return NULL;
  else {
    char *res = TAB_CREATE(char, la+lb+1), *iter;
    
    if( NULL!=res ) {
      iter = res;
      
      if( la )
	for( ; (*iter=*a); ++a, ++iter);
      if( lb )
	for( ; (*iter=*b); ++b, ++iter);
    }
    return res;
  }
}

size_t string_length(char const *s) {
  size_t res = 0;
  if( NULL==s ) 
    return 0;
  for( ; *s; ++s, ++res);
  return res;
}

int string_cmp(char const *a, char const *b) {
  if( NULL==a )
    return (NULL==b || '\0'==*b)?0:-1;
  else if( NULL==b )
    return ('\0'==*a)?0:1;
  else {
    int res;
    
    for( ; (res=(int)*a-(int)*b)==0 && *a; ++a, ++b);
    return res;
  }
}

size_t string_hash(char const *s) {
  if( NULL==s )
    return 0;
  else {
    unsigned long long res = 0L;
    
    for( ; *s; ++s )
      res = 5*res+(int)*s;
    return (size_t)res;
  }
}

int string_starts_p(char const *main , char const *deb) {
  if( NULL==deb || '\0'==*deb )
    return 1;
  if( NULL==main )
    return 0;
  for( ; *deb  && *main==*deb; ++main, ++deb);
  return '\0'==*deb;
}

char const *string_find(char const *str, char const *sub) {
  if( NULL!=str )
    for( ; *str && !string_starts_p(str, sub); ++str);
  return str;
}

