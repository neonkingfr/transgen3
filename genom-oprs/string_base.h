/* -*- c -*-
 * $Id: string_base.h,v 1.3 2004/07/05 14:02:14 fpy Exp $
 *
 * Author(s):
 *  - Frédéric Py <fpy@laas.fr>
 *
 * Creation Date: Jun 05, 2004
 */
#ifndef R2C_STRING_BASE_HEADER
#define R2C_STRING_BASE_HEADER

#include "memory.h"

char *string_dup(char const *a);
char *string_cat(char const *a, char const *b);

char *sub_string(char const *from, char const *to);

size_t string_length(char const *s);
int string_cmp(char const *a, char const *b);

size_t string_hash(char const *s);

int string_starts_p(char const *main , char const *deb);
char const *string_find(char const *str, char const *sub);

#endif
