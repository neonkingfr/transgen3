/* -*- java -*-
 * $Id: IDGenerator.java,v 1.1 2004/09/30 09:36:44 fpy Exp $
 *
 * Author(s):
 *   - Frédéric Py <fpy@laas.fr>
 *
 * Creation Date: Aug 31, 2004
 */

public class IDGenerator {

    private static int counter;

    static String nextID() {
	return String.valueOf(counter++);
    }

}
