/* -*- java -*-
 * $Id: R2CAlert.java,v 1.1 2004/10/22 21:54:05 fpy Exp $
 *
 * Author(s):
 *   - Frédéric Py <fpy@laas.fr>
 *
 * Creation Date: Oct 22, 2004
 */
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;

public class R2CAlert extends JDialog {
    private JTextArea longMsg;
    private JScrollPane scroll;

    public R2CAlert(Frame parent, boolean modal) {
	super(parent, modal);
	initComponents();
    }

    private void closeDialog() {
	setVisible(false);
	hide();
    }

    public void setExcept(Exception e) {
	StringWriter strw = new StringWriter();
	PrintWriter istr = new PrintWriter(strw);
	e.printStackTrace(istr);
	longMsg.setText(strw.toString());
	pack();
    }

    private void initComponents() {
	JButton okButton;
	GridBagConstraints constraints;

	setTitle("Exception");
	addWindowListener(new WindowAdapter() {
		public void windowClosing(WindowEvent ev) {
		    closeDialog();
		}
	    });
	getContentPane().setLayout(new GridBagLayout());


	longMsg= new JTextArea();
	longMsg.setEditable(false);
	longMsg.setText("");
// 	longMsg.setPreferredSize(new Dimension(500, 200));
	scroll = new JScrollPane();
	scroll.setViewportView(longMsg);
	constraints = new GridBagConstraints();
	constraints.gridx = 0;
	constraints.gridy = 0;
	constraints.gridwidth = 2;
	constraints.weighty = 1.0;
	constraints.weightx = 1.0;
	constraints.fill = GridBagConstraints.BOTH;
	getContentPane().add(scroll, constraints);


	okButton = new JButton();
	okButton.setText("OK");
	okButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ev) {
		    closeDialog();
		}
	    });
	constraints = new GridBagConstraints();
	constraints.gridx = 1;
	constraints.gridy = 1;
	constraints.weighty = 0.0;
	constraints.weightx = 0.5;
	constraints.fill = GridBagConstraints.BOTH;
	getContentPane().add(okButton, constraints);

    }

}
