/* -*- java -*-
 * $Id: R2CConnect.java,v 1.1 2004/08/31 18:30:40 fpy Exp $
 *
 * Author(s):
 *   - Frédéric Py <fpy@laas.fr>
 *
 * Creation Date: Aug 30, 2004
 */
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;

public class R2CConnect extends JDialog {
    public R2CConnect(Frame parent, boolean modal) {
	super(parent, modal);
	initComponents();
    }

    private void initComponents() {
	hostLabel = new JLabel();
	portLabel = new JLabel();
	hostField = new JTextField();
	portField = new JTextField();
	okButton = new JButton();
	cancelButton = new JButton();

	getContentPane().setLayout(new GridBagLayout());
	GridBagConstraints constraints;

	setTitle("Connect to R2C");
	setModal(true);
	setResizable(false);
	addWindowListener(new WindowAdapter() {
		public void windowClosing(WindowEvent ev) {
		    closeDialog(false);
		}
	    });

	hostLabel.setText("Host :");
	constraints = new GridBagConstraints();
	constraints.gridx = 0;
	constraints.gridy = 0;
	constraints.fill = GridBagConstraints.BOTH;
	getContentPane().add(hostLabel, constraints);

	hostField.setText("dala");
	constraints = new GridBagConstraints();
	constraints.gridx = 1;
	constraints.gridy = 0;
	constraints.gridwidth = 2;
	constraints.weightx = 1.0;
	constraints.fill = GridBagConstraints.BOTH;
	getContentPane().add(hostField, constraints);


	portLabel.setText("Port :");
	constraints = new GridBagConstraints();
	constraints.gridx = 0;
	constraints.gridy = 1;
	constraints.fill = GridBagConstraints.BOTH;
	getContentPane().add(hostLabel, constraints);

	portField.setText("3140");
	constraints = new GridBagConstraints();
	constraints.gridx = 1;
	constraints.gridy = 1;
	constraints.gridwidth = 2;
	constraints.weightx = 1.0;
	constraints.fill = GridBagConstraints.BOTH;
	getContentPane().add(portField, constraints);

	cancelButton.setText("Cancel");
	cancelButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ev) {
		    closeDialog(false);
		}
	    });
	constraints = new GridBagConstraints();
	constraints.gridx = 1;
	constraints.gridy = 2;
	constraints.fill = GridBagConstraints.BOTH;
	getContentPane().add(cancelButton, constraints);

	okButton.setText("Connect");
	okButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ev) {
		    closeDialog(true);
		}
	    });
	constraints = new GridBagConstraints();
	constraints.gridx = 2;
	constraints.gridy = 2;
	constraints.fill = GridBagConstraints.BOTH;
	getContentPane().add(okButton, constraints);
	pack();
    }

    private void closeDialog(boolean ok) {
	isOK = ok;
	setVisible(false);
	dispose();
    }

    public boolean isReplyOK() {
	return isOK;
    }

    public String getHostName()
	throws IOException {
	if( !isOK )
	    throw new IOException("Dialog canceled");
	return hostField.getText();
    }

    public int getPort()
	throws Exception {
	if( !isOK )
	    throw new IOException("Dialog canceled");
	return Integer.parseInt(portField.getText());
    }

    private boolean isOK;

    private JLabel hostLabel, portLabel;
    private JTextField hostField, portField;
    private JButton okButton, cancelButton;
}
