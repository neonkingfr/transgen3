/* -*- java -*-
 * $Id: R2CInterf.java,v 1.32 2005/10/17 09:09:47 fpy Exp $
 *
 * Author(s):
 *   - Frédéric Py <fpy@laas.fr>
 *
 * Creation Date: Oct 04, 2004
 */
import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.text.Document;

import java.awt.*;
import java.awt.event.*;

import java.text.SimpleDateFormat;
import java.util.*;

import org.jfree.chart.*;
import org.jfree.chart.renderer.category.*;
import org.jfree.data.gantt.*;
import org.jfree.data.time.*;
import org.jfree.chart.axis.*;
import org.jfree.chart.plot.*;
import org.jfree.util.*;

public class R2CInterf extends JFrame implements ActionListener {

    private class StatTable extends AbstractTableModel {

	private class StatInfo {
	    private long number;
	    private long min, max, avg;

	    private SimpleDateFormat
		formatDur = new SimpleDateFormat("mm ss.SSS");

	    public StatInfo() {
		number = 0;
	    }

	    public void addDuration(Date from, Date to) {
		long duration = to.getTime()-from.getTime();

		if( 0==number )
		    min = avg = max = duration;
		else {
		    if( min>duration )
			min = duration;
		    else if( max<duration )
			max = duration;
		    avg = (number*avg + duration)/(number+1);
		}
		number += 1;
	    }

	    private String getDate(long value) {
		if( 0==number )
		    return new String();
		else
		    return formatDur.format(new Date(value));
	    }

	    public Object get(String name, int pos) {
		switch( pos ) {
		case 0:
		    return name;
		case 1:
		    return getDate(min);
		case 2:
		    return getDate(avg);
		case 3:
		    return getDate(max);
		case 4:
		    return new Long(number);
		default:
		    return null;
		}
	    }

	    public int getCount() {
		return 5;
	    }

	    public String getName(int pos) {
		switch( pos ) {
		case 0:
		    return "Task";
		case 1:
		    return "Min";
		case 2:
		    return "Avg";
		case 3:
		    return "Max";
		case 4:
		    return "Count";
		default:
		    return null;
		}
	    }

	} // class R2CInterf.StatTable.StatInfo

	private TreeMap dataTable;

	public StatTable() {
	    dataTable = new TreeMap();
	}

	public int getRowCount() {
	    return dataTable.size();
	}

	public int getColumnCount() {
	    StatInfo tmp = new StatInfo();
	    return tmp.getCount();
	}

	public String getColumnName(int col) {
	    StatInfo tmp = new StatInfo();
	    return tmp.getName(col);
	}

	public Object getValueAt(int row, int col) {
	    String task = (String)dataTable.keySet().toArray()[row];
	    return ((StatInfo)dataTable.get(task)).get(task, col);
	}

	public void addDuration(String task, Date from, Date to) {
	    StatInfo info = (StatInfo)dataTable.get(task);

	    if( null==info ) {
		info = new StatInfo();
		dataTable.put(task, info);
	    }
	    info.addDuration(from, to);
	    fireTableDataChanged();
	}

    } // class R2CInterf.StatTable


    private R2CSocket socketIn;
    private StatTable timeDatas;
    private Vector    tickDates;
    private long tickRange;

    private Map runningTasks, trueFacts;
    private LinkedList finishedTasks;
    private HashMap   knownTasks, taskType, knownFacts;

    /* Visual componnents */
    private javax.swing.Timer timer;
    private JMenuItem connectItem, disconnectItem, setRangeItem;

    private JTextArea msgLog;
    private R2CAlert  alert;

    private TaskSeries tasks;
    private DateAxis taskAxis;

    private void initComponents() {
	JMenuBar menuBar = new JMenuBar();
	JMenu menu = new JMenu();

	//- Creation of the menu
	connectItem = new JMenuItem();
	connectItem.setText("Connect ...");
	connectItem.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ev) {
		    connect();
		}
	    });
	menu.add(connectItem);

	disconnectItem = new JMenuItem();
	disconnectItem.setText("Disconnect");
	disconnectItem.setEnabled(false);
	disconnectItem.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ev) {
		    disconnect();
		}
	    });
	menu.add(disconnectItem);

	setRangeItem = new JMenuItem();
	setRangeItem.setText("Visual range ...");
	setRangeItem.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ev) {
		    setRange();
		}});
	menu.add(setRangeItem);

	menu.add(new JSeparator());

	JMenuItem item = new JMenuItem();
	item.setText("Quit");
	item.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ev) {
		    quit(0);
		}
	    });
	menu.add(item);
	menu.setText("Menu");
	menuBar.add(menu);
	setJMenuBar(menuBar);

	//- window components

	JTabbedPane pane = new JTabbedPane();
	JFreeChart chart;
	ChartPanel view;
	TaskSeriesCollection series;
	CategoryPlot plot;
	CategoryAxis axis;

	JPanel ipane = new JPanel(new GridLayout(0,1));

	// chart for tasks

	series = new TaskSeriesCollection();
	tasks = new TaskSeries("Tasks");
	series.add(tasks);
	chart = ChartFactory.createGanttChart(null, null, null, series,
					      false, false, false);
	chart.setBorderVisible(false);
	chart.setBackgroundPaint(new GradientPaint(0, 0, Color.white,
						   1000, 0, Color.blue));
	plot = chart.getCategoryPlot();
	plot.setForegroundAlpha(0.5f);
	plot.getRenderer().setSeriesPaint(0, Color.blue);
	((BarRenderer)(plot.getRenderer())).setShadowVisible(true);
	plot.setDomainGridlinesVisible(true);

	axis = plot.getDomainAxis();
	axis.setTickLabelFont(new Font("fixed", Font.PLAIN, 10));
	axis.setMaximumCategoryLabelWidthRatio(10.0f);
	axis.setLabel("Tasks");

	taskAxis = (DateAxis)plot.getRangeAxis();
	taskAxis.setDateFormatOverride(new SimpleDateFormat("ssSSS"));

	view = new ChartPanel(chart);
	view.setPreferredSize(new Dimension(500, 400));
	view.setMouseZoomable(true, false);
	view.restoreAutoBounds();
	view.setRangeZoomable(false);

	pane.addTab("Graphics", null, view, "Graphical display");

	JTable table = new JTable(timeDatas);
	JScrollPane scroll = new JScrollPane();
	scroll.setViewportView(table);
	pane.addTab("Time stats", null, scroll, "Task durations");


	msgLog = new JTextArea();
	msgLog.setEditable(false);
	scroll = new JScrollPane();
	scroll.setViewportView(msgLog);
	pane.addTab("Messages", null, scroll, "Messages received");

	getContentPane().add(pane, BorderLayout.CENTER);

	//- extras
	setTitle("OPRS-GenoM Interface");
	addWindowListener(new WindowAdapter() {
		public void windowClosing(WindowEvent e) {
		    quit(0);
		}
	    });
	timer = new javax.swing.Timer(200, this);
	pack();

	alert = new R2CAlert(this, true);
    }

    private void connect() {
	R2CConnect connector = new R2CConnect(this, true);
	boolean success;

	do {
	    tickDates.clear();
	    runningTasks.clear();
	    trueFacts.clear();
	    knownFacts.clear();
	    taskType.clear();
	    knownTasks.clear();

	    tasks.removeAll();
	    finishedTasks.clear();

	    connectItem.setEnabled(false);
	    success = true;
	    connector.setVisible(true);
	    if( connector.isReplyOK() ) {
		try {
		    socketIn.connect(connector.getHostName(),
				     connector.getPort());
		    timer.start();
		    setTitle("OPRS-GenoM Interface on "+connector.getHostName());
		    disconnectItem.setEnabled(true);
		} catch(Exception e) {
		    success = false;
		    alert.setExcept(e);
		    alert.setVisible(true);
		}
	    } else
		connectItem.setEnabled(true);
	} while( !success );
    }

    private void disconnect() {
	disconnectItem.setEnabled(false);
	try {
	    socketIn.close();
	    timer.stop();
	    setTitle("OPRS-GenoM Interface");
	    connectItem.setEnabled(true);
	} catch(Exception e) {
	    alert.setExcept(e);
	    alert.setVisible(true);
	}
    }

    private void quit(int ret) {
	disconnect();
	System.exit(ret);
    }

    private void refreshViews() {
	long lastDate = tickDates.size();
	Date min, max;

	if( tickRange==0 ) {
	    min = new Date(0);
	    max = new Date(lastDate);
	} else if( lastDate<=tickRange ) {
	    min = new Date(0);
	    max = new Date(lastDate);
	} else {
	    max = new Date(lastDate);
	    min = new Date(lastDate-tickRange);
	}

	taskAxis.setMinimumDate(min);
	taskAxis.setMaximumDate(max);
    }


    private void newTick(String info) {
	int spacePos =  info.indexOf(' ');
	long lastTick = tickDates.size()-1,
	    tickNum = Long.valueOf(info.substring(0, spacePos)).longValue(),
	    date = Long.valueOf(info.substring(spacePos+1,
					       info.length()-3)).longValue();
	Date  theDate = new Date(date), numDate = new Date(tickNum);


	if( tickNum>lastTick ) {
	    tickDates.add(theDate);

	    addLog("New Tick : "+tickNum);
	    refreshViews();

	    if( lastTick>=0 ) {
		Iterator iter;

		timeDatas.addDuration(" Interface event",
				      (Date)tickDates.get((int)lastTick),
				      theDate);
		iter = runningTasks.values().iterator();

		while( iter.hasNext() ) {
		    Task cur = (Task)iter.next();
		    TimePeriod dur = cur.getDuration();

		    cur.setDuration(new SimpleTimePeriod(dur.getStart(),
							 numDate));
		}

		while( !finishedTasks.isEmpty() ) {
		    String id = (String)finishedTasks.removeFirst();
		    Task tsk = (Task)runningTasks.remove(id);
		    if (null != tsk) {
                        TimePeriod dur = tsk.getDuration();
                        long begTick = dur.getStart().getTime();

                        timeDatas.addDuration((String)taskType.remove(id),
                                              (Date)tickDates.get((int)begTick),
                                              theDate);
                    }
		}

		iter = trueFacts.values().iterator();
		while( iter.hasNext() ) {
		    Task cur = (Task)iter.next();
		    TimePeriod dur = cur.getDuration();

		    cur.setDuration(new SimpleTimePeriod(dur.getStart(),
							 numDate));
		}
	    }
	}
    }

    private void startTask(String info) {
	int spacePos = info.indexOf(' '),
	    tickNum = tickDates.size();
	Date numDate = new Date(tickNum);
	String type = info.substring(0, spacePos),
	    id = info.substring(spacePos+1);
	Task tsk = (Task)knownTasks.get(type),
	    subTsk = new Task(id, numDate, numDate);

	taskType.put(id, type);
	if( null==tsk ) {
	    tsk = new Task(type, numDate, numDate);
	    knownTasks.put(type, tsk);
	    tasks.add(tsk);
	}
	subTsk.setPercentComplete(0.5);
	tsk.addSubtask(subTsk);
	runningTasks.put(id, subTsk);
    }

    private void endTask(String info) {
	int spacePos = info.indexOf(' ');
	String id = info.substring(0, spacePos);
	String report = info.substring(spacePos + 2);
        int quotePos = report.indexOf('"');
        report = report.substring(0, quotePos);

        Task tsk = (Task)runningTasks.remove(id);

	if( null!=tsk ) {
	    System.out.println(report);
	    if (report.equals("OK") || report.contains("_stdGenoM_ACTIVITY_INTERRUPTED")) {
	        tsk.setPercentComplete(1.0);
                System.out.println("finished correctly");
            } else {
                tsk.setPercentComplete(0.0);
                System.out.println("finished with error");
            }
            taskType.remove(id);
	}
    }

    private void r2cEnd(String id) {
	Task tsk = (Task)runningTasks.remove(id);

	if( null!=tsk ) {
	    taskType.remove(id);
	    tsk.setPercentComplete(0.0);
	}
    }

    private void r2cTrue(String fact) {
	Task subTsk = (Task)trueFacts.get(fact);

	if( null==subTsk ) {
	    Task factT = (Task)knownFacts.get(fact);
	    Date lastTick = new Date(tickDates.size());

	    if( null==factT ) {
		factT = new Task(fact, lastTick, lastTick);
		knownFacts.put(fact, factT);
	    }
	    subTsk = new Task(IDGenerator.nextID(), lastTick, lastTick);
	    factT.addSubtask(subTsk);
	    trueFacts.put(fact, subTsk);
	}
    }

    private void r2cFalse(String fact) {
	trueFacts.remove(fact);
    }

    private void addLog(String message) {
	Document doc = msgLog.getDocument();
	int carret = msgLog.getCaretPosition(),
	    size = doc.getEndPosition().getOffset();
	msgLog.append("tick "+tickDates.size()+": "+message+'\n');

	if( carret==size )
	    msgLog.setCaretPosition(doc.getEndPosition().getOffset());
    }


    private void treatMessage(String msg) {
	addLog(msg);
	if( msg.startsWith("TICK ") ) {
	    newTick(msg.substring(5));
	} else {
	    if( msg.startsWith("RQ ") )
		startTask(msg.substring(3));
	    else if( msg.startsWith("FR ") )
		endTask(msg.substring(3));
	    else if( msg.startsWith("REJ ") )
		r2cEnd(msg.substring(4));
	    else if( msg.startsWith("KILL ") )
		r2cEnd(msg.substring(5));
	    else if( msg.startsWith("FALSE ") )
		r2cFalse(msg.substring(6));
	    else if( msg.startsWith("TRUE ") )
		r2cTrue(msg.substring(5));
	    else
		System.err.println("Unknown message : "+msg);
	}
    }


    private void setRange() {
	R2CRange rangeSelect = new R2CRange(this, true, tickRange);
	rangeSelect.setVisible(true);
	tickRange = rangeSelect.getValue();
	refreshViews();
    }

    public R2CInterf() {
	socketIn = new R2CSocket();
	timeDatas = new StatTable();
	tickDates = new Vector();
	tickRange = 1300;


	knownTasks = new HashMap();
	finishedTasks = new LinkedList();
	taskType = new HashMap();
	runningTasks = Collections.synchronizedMap(new HashMap());
	trueFacts = Collections.synchronizedMap(new HashMap());
	knownFacts = new HashMap();

	initComponents();
    }

    public void actionPerformed(ActionEvent ev) {
	try {
	    String message;

	    while( socketIn.hasMessage() ) {
		message = socketIn.getMessage();
		treatMessage(message.substring(1, message.length()-1));
	    }
	} catch(Exception e) {
	    alert.setExcept(e);
	    alert.setVisible(true);
	}
    }

    private static void setMacLF() {
	// This line is for Mac style menus under macos
	System.setProperty("apple.laf.useScreenMenuBar", "true");
	System.setProperty("com.apple.mrj.application.live-resize", "true");
    }


    public static void main(String args[]) {
	setMacLF();
	new R2CInterf().setVisible(true);
    }

} // class R2CInterf

