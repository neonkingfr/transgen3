/* -*- java -*-
 * $Id: R2CRange.java,v 1.2 2004/11/16 15:05:09 fpy Exp $
 *
 * Author(s):
 *   - Frédéric Py <fpy@laas.fr>
 *
 * Creation Date: Nov 16, 2004
 */
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;

public class R2CRange extends JDialog {

    private long value;

    public R2CRange(Frame parent, boolean modal, long current) {
	super(parent, modal);
	initComponents(current);
    }

    private void initComponents(long val) {
	value = val;
	okButton = new JButton();
	cancelButton = new JButton();
	rangeLabel = new JLabel();
	valField = new JTextField();

	getContentPane().setLayout(new GridBagLayout());
	GridBagConstraints cstr;

	setTitle("Set visual range");
	setModal(true);
	setResizable(false);

	addWindowListener(new WindowAdapter() {
		public void windowClosing(WindowEvent e) {
		    closeDialog(false);
		}
	    });
	rangeLabel.setText("Visual range :");
	cstr = new GridBagConstraints();
	cstr.gridx = 0;
	cstr.gridy = 0;
	cstr.fill = GridBagConstraints.BOTH;
	getContentPane().add(rangeLabel, cstr);

	valField.setText(Long.toString(value));
	cstr = new GridBagConstraints();
	cstr.gridx = 1;
	cstr.gridy = 0;
	cstr.fill = GridBagConstraints.BOTH;
	cstr.gridwidth = 2;
	getContentPane().add(valField, cstr);

	cancelButton.setText("Cancel");
	cancelButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ev) {
		    closeDialog(false);
		}
	    });
	cstr = new GridBagConstraints();
	cstr.gridx = 1;
	cstr.gridy = 1;
	cstr.fill = GridBagConstraints.BOTH;
	getContentPane().add(cancelButton, cstr);

	okButton.setText("OK");
	okButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ev) {
		    closeDialog(true);
		}
	    });
	cstr = new GridBagConstraints();
	cstr.gridx = 2;
	cstr.gridy = 1;
	cstr.fill = GridBagConstraints.BOTH;
	getContentPane().add(okButton, cstr);
	pack();
    }


    private void closeDialog(boolean isOk) {
	R2CAlert alert = new R2CAlert((Frame)getOwner(), true);
	if( isOk ) {
	    try {
		value = Long.parseLong(valField.getText());
	    } catch(Exception e) {
		alert.setExcept(e);
		alert.show();
	    }
	}
	setVisible(false);
	dispose();
    }

    public long getValue() {
	return value;
    }


    private JButton okButton, cancelButton;
    private JLabel rangeLabel;
    private JTextField valField;
}
