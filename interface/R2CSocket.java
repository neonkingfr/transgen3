/* -*- java -*-
 * $Id: R2CSocket.java,v 1.2 2005/02/09 13:06:15 fpy Exp $
 *
 * Author(s):
 *   - Frédéric Py <fpy@laas.fr>
 *
 * Creation Date: Oct 04, 2004
 */

import java.net.*;
import java.io.*;

public class R2CSocket {
    private Socket r2cSocket = null;
    private DataInputStream dis;

    public R2CSocket() {}

    public R2CSocket(String hostName, int port)
	throws UnknownHostException, IOException {
	connect(hostName, port);
    }

    public void finalize()
	throws Throwable {
	close();
	super.finalize();
    }

    public void connect(String hostName, int port)
	throws UnknownHostException, IOException {
	if( isConnected() )
	    throw new ConnectException("Already connected.");
	Socket tmp = new Socket(hostName, port);
	dis = new DataInputStream(tmp.getInputStream());
	r2cSocket = tmp;
    }

    public void close()
	throws IOException {
	if( isConnected() ) {
	    Socket tmp = r2cSocket;
	    dis = null;
	    r2cSocket = null;
	    tmp.close();
	}
    }

    public boolean isConnected() {
	return r2cSocket!=null;
    }

    private final int getInt()
	throws IOException {
	return dis.readInt();
    }

    private final String getString()
	throws IOException {
	int size = getInt();

	if( size>0 ) {
	    byte[] buff = new byte[size];
	    dis.read(buff);
	    return new String(buff);
	} else
	    return new String();
    }

    public boolean hasMessage()
	throws IOException {
	if( !isConnected() )
	    throw new IOException("Not connected.");
	return dis.available()>0;
    }

    public String getMessage()
	throws IOException {
	if( !hasMessage() )
	    throw new IOException("No message.");
	return waitForMessage();
    }

    public String waitForMessage()
	throws IOException {
	return getString();
    }

} // class R2CSocket
