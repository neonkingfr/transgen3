static const char* const rcsid = "$Id$";
/*                               -*- Mode: C -*- 
 * data-tg.c -- 
 * 
 * Copyright (C) 1999-2012 LAAS/CNRS
 * 
 * Author          : Francois Felix Ingrand
 * Created On      : Fri Jul  9 10:07:12 1999
 * Last Modified By: Francois Felix Ingrand
 * Last Modified On: Fri Sep 17 17:46:11 1999
 * Update Count    : 30
 * Status          : OK
 * 
 * $Id$
 */

#include "config.h"

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "slistPack.h"
#include "slistPack_f.h"

#include "macro.h"
#include "transgen.h"
#include "transgen_f.h"
#include "perl-build_f.h"


#define SCRIPT_IN "auto/superv.input"
#define SCRIPT_OUT "auto/%s-superv.input"
#define XSCRIPT_IN "auto/xsuperv.input"
#define XSCRIPT_OUT "auto/%s-xsuperv.input"
#define DATA_INC_MOD "auto/data.inc.mod"
#define DATA_INC_BEGIN "auto/data.inc.begin"
#define DATA_INC_MOD "auto/data.inc.mod"
#define DATA_INC_END "auto/data.inc.end"
#define DATA_INC_OUT "auto/%s-superv.inc.input"


PBoolean data_transgen(FILE *out)
{
     Module *module;

     script_open(out);

     subst_begin(out, DATA_INC_BEGIN);
     subst_end(out);


     sl_loop_through_slist(supervisor->mod_list, module, Module *) {
	  subst_begin(out, DATA_INC_MOD);
	  print_sed_subst(out, "module", module->name);
	  print_sed_subst(out, "MODULE", module->NAME_SHV);
	  subst_end(out);
     }

     subst_begin(out, DATA_INC_END);
     subst_end(out);

     script_close(out, DATA_INC_OUT, supervisor->name);

     script_open(out);

     subst_begin(out, SCRIPT_IN);
     print_sed_subst(out, "superv", supervisor->name);
     subst_end(out);

     script_close(out, SCRIPT_OUT, supervisor->name);
     script_open(out);

     subst_begin(out, XSCRIPT_IN);
     print_sed_subst(out, "superv", supervisor->name);
     subst_end(out);

     script_close(out, XSCRIPT_OUT, supervisor->name);

     return TRUE;
}
