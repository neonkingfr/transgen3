/*                               -*- Mode: C -*- 
 * macro.h
 * 
 * Copyright (C) 1999-2012 LAAS/CNRS
 * 
 * Author          : Francois Felix Ingrand
 * Created On      : Mon Jun 28 14:59:37 1999
 * Last Modified By: Francois Felix Ingrand
 * Last Modified On: Thu Jul 15 16:12:42 1999
 * Update Count    : 10
 * Status          : OK
 * 
 * $Id$
 */

#define MALLOC(x) (malloc(x))
#define REALLOC(x,y) (realloc(x,y))
#define FREE(ptr) (free(ptr))


#define NEW_STR(to, from) do {int len = strlen(from);\
				   to = (char *)MALLOC(len+1);\
				   memcpy(to, from, len+1);\
				   } while (0)

#define UPPERCASE_STRING_IN_PLACE(from)  do {int i;\
     for (i = 0; from[i] != '\0'; from[i] = toupper(from[i]), i++);} while (0)

#define UPPERCASE_STRING_IN_PLACE_RPL_MINUS(from)  do {int i;\
	  for (i = 0; from[i] != '\0'; from[i] = toupper(from[i]), i++)\
	       if (from[i-1]=='-') from[i-1]= '_';} while (0)


#define NEW_QSTR(to, from) do {int len = strlen(from) - 2;\
				   to = (char *)MALLOC(len+1);\
				   memcpy(to, from + 1, len);\
				   to[len] = '\0';\
				   } while (0)

#define MAKE_OBJECT(type) (type *)malloc(sizeof(type))

#define FREE_OBJECT(ptr) (free(ptr))


