static const char* const rcsid = "$Id$";
/*                               -*- Mode: C -*- 
 * make-tg.c -- 
 * 
 * Copyright (C) 1999-2013 LAAS/CNRS
 * 
 * Author          : Francois Felix Ingrand
 * Created On      : Mon Jul 19 16:33:48 1999
 * Last Modified By: Francois Felix Ingrand
 * Last Modified On: Sat Sep  4 16:16:50 1999
 * Update Count    : 14
 * Status          : OK
 * 
 */

#include "config.h"

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "slistPack.h"
#include "slistPack_f.h"

#include "macro.h"
#include "transgen.h"
#include "transgen_f.h"
#include "perl-build_f.h"

#define MAKEFILE_BEGIN "auto/Makefile.am.begin"
#define MAKEFILE_MOD "auto/Makefile.am.mod"
#define MAKEFILE_END "auto/Makefile.am.end"
#define MAKEFILE_OUT "auto/Makefile.am"

#define CONFIGURE_AC_BEGIN "configure.ac.begin"
#define CONFIGURE_AC_MOD "configure.ac.mod"
#define CONFIGURE_AC_END "configure.ac.end"
#define CONFIGURE_AC_OUT "configure.ac"



PBoolean make_transgen(FILE *out)
{
     Module *module;

     script_open(out);

     subst_begin(out, MAKEFILE_BEGIN);
     print_sed_subst(out, "superv-name", supervisor->name);
     subst_end(out);


     sl_loop_through_slist(supervisor->mod_list, module, Module *) {
	  subst_begin(out, MAKEFILE_MOD);
	  print_sed_subst(out, "module", module->name);
	  print_sed_subst(out, "MODULE", module->NAME_SHV);
	  subst_end(out);
     }

     subst_begin(out, MAKEFILE_END);
     print_sed_subst(out, "superv-name", supervisor->name);
     subst_end(out);

     script_close(out, MAKEFILE_OUT);

     return TRUE;
}

PBoolean configure_transgen(FILE *out)
{
     Module *module;

     script_open(out);

     subst_begin(out, CONFIGURE_AC_BEGIN);
     print_sed_subst(out, "superv-name", supervisor->name);
     subst_end(out);


     sl_loop_through_slist(supervisor->mod_list, module, Module *) {
	  subst_begin(out, CONFIGURE_AC_MOD);
	  print_sed_subst(out, "module", module->name);
	  print_sed_subst(out, "MODULE", module->NAME_SHV);
	  subst_end(out);
     }

     subst_begin(out, CONFIGURE_AC_END);
     //    print_sed_subst(out, "superv-name", supervisor->name);
     subst_end(out);

     script_close(out, CONFIGURE_AC_OUT);

     return TRUE;
}
