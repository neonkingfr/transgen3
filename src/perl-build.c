static const char* const rcsid = "$Id$";

/*                               -*- Mode: C -*- 
 * perl-build.c -- 
 * 
 * Copyright (C) 1999 LAAS/CNRS
 * 
 * Author          : Stolen from Genom...
 * Created On      : Fri Jul  2 14:32:57 1999
 * Last Modified By: Francois Felix Ingrand
 * Last Modified On: Tue Jul 27 15:03:21 1999
 * Update Count    : 17
 * Status          : OK
 * 
 */


#include "config.h"

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <ctype.h>
#include <sys/param.h>


#include "slistPack.h"
#include "slistPack_f.h"

#include "macro.h"
#include "transgen.h"
#include "perl-build_f.h"

/*----------------------------------------------------------------------*/

/**
 ** Construction incrementale de chaine 
 **/
void 
bufcat(char **buf, char *fmt, ...)
{
    va_list ap;
    static char buf1[1024];
    int taille;

    if (*buf == NULL) {
	*buf = MALLOC(sizeof(char));
	*buf[0] = '\0';
	taille = sizeof(char);
    } else {
	taille = strlen(*buf) + 1;
    }

    va_start(ap, fmt);
    vsprintf(buf1, fmt, ap);
    taille += strlen(buf1);
    
    *buf = REALLOC(*buf, taille);
    strcat(*buf, buf1);
} /* bufcat */


/**
 ** Appel buffcat si la chaine a ajouter n'est pas deja dans buf
 ** Sinon retourne 0
 **/

int
bufcatIfNotIn(char **buf, char *fmt, ...)
{
  va_list ap;
  static char buf1[1024];

  va_start(ap, fmt);
  vsprintf(buf1, fmt, ap);
  
  if (*buf == NULL || strstr(*buf, buf1) == NULL) {
    bufcat(buf, buf1);
    return 1;
  }
  return 0;
}

/*----------------------------------------------------------------------*/
    
/**
 ** Ge'ne'ration des commandes pour construire les substitutions
 **/

void 
print_sed_subst(FILE *f, char *from, char *fmt, ...) 
{
    va_list ap;
    char *f1 = "    s^\\$";
    char *f2 = "\\$^";
    char *f3 = "^g;\n";
    int size;
    char *format;
    
    va_start(ap, fmt);

    size = strlen(f1) + (from != NULL ? strlen(from) : 0) + strlen(f2) 
	+ ( fmt != NULL ? strlen(fmt) : 0) + strlen(f3) + 1;    

    format = MALLOC(size);
    strcpy(format, f1);
    if (from != NULL)
	strcat(format, from);
    strcat(format, f2);
    if (fmt != NULL)
	strcat(format, fmt);
    strcat(format, f3);

    vfprintf(f, format, ap);

    free(format);

    va_end(ap);
} /* print_sed_subst */

/*----------------------------------------------------------------------*/

/**
 ** genere l'ouverture d'un nouveau fichier
 **/
void
script_open(FILE *out)
{	
    fprintf(out, "open (OUT,\">/tmp/gen$$\") "
	    "|| die \"Can't open output\";\n");
}

/*----------------------------------------------------------------------*/

/**
 ** Genere la fermeture d'un fichier et le 'move-if-change' final  
 **/
void
script_close(FILE *out, char *newName,...)
{
    va_list ap;
    char buf[1024];
    
    va_start(ap, newName);
    
    vsprintf(buf, newName, ap);
    fprintf(out, "close OUT;\n");
    fprintf(out, "move_if_change(\"/tmp/gen$$\", \"%s\");\n", buf);
    fprintf(out, "\n");
    va_end(ap);
}

/*----------------------------------------------------------------------*/

/** 
 ** Genere l'execution d'un script
 **
 ** (permet de recupperer un "die" )
 **/
void script_do(FILE *out, char *dir, char *doFileName)
{
  fprintf(out, "#-----------------------------------"
	  "-----------------------------------\n\n"
	  "do '%s/%s';\n"
	  "if ($@) {\n"
	  "   die \"Error: $@ \\n\";\n"
	  "}\n\n", dir, doFileName);
  fprintf(out, "#-----------------------------------"
	  "-----------------------------------\n\n");
}
   
/*----------------------------------------------------------------------*/

/** 
 ** Genere le debut d'une substitution 
 **/
void
subst_begin(FILE *out, char *protoName)
{
    
    fprintf(out, "open (IN,\"%s/%s\") \n\t|| die \"Can't open skeleton %s/%s\";\n", 
	    canvas_dir, protoName, canvas_dir, protoName);
    fprintf(out, "while (<IN>) {\n    chop;\n");
}
    
/*----------------------------------------------------------------------*/

/**
 ** Genere la fin d'une substitution
 **/
void
subst_end(FILE *out)
{
    
    fprintf(out, "    print OUT;\n");
    fprintf(out, "}\n");
    fprintf(out, "close IN;\n");
}


/*----------------------------------------------------------------------*/

/**
 ** Genere le debut d'une insertion litterale
 **/
   
void 
cat_begin(FILE *out)
{
    fprintf(out, "print OUT <<'FIN_DE_CAT';\n");
}

/*----------------------------------------------------------------------*/

/**
 ** Genere la fin d'une intertion litterale 
 **/
void
cat_end(FILE *out)
{
    fprintf(out, "FIN_DE_CAT\n");
}


PBoolean relative_path(char *path)
{
     return(path && (path[0]!='/'));
}
