/*                               -*- Mode: C -*- 
 * perl-build_f.h -- 
 * 
 * Copyright (C) 1999 LAAS/CNRS
 * 
 * Author          : Francois Felix Ingrand
 * Created On      : Thu Jul  8 14:48:02 1999
 * Last Modified By: Francois Felix Ingrand
 * Last Modified On: Tue Jul 27 14:26:40 1999
 * Update Count    : 5
 * Status          : OK
 * 
 * $Id$
 */

void script_open(FILE *out);
void script_do(FILE *out, char *dir, char *doFileName);
void script_close(FILE *out, char *newName,...);

void subst_begin(FILE *out, char *protoName);
void subst_end(FILE *out);

void print_sed_subst(FILE *f, char *from, char *fmt, ...);
void bufcat(char **buf, char *fmt, ...);

PBoolean relative_path(char *path);
