%option noyywrap
%{

#include "config.h"

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "slistPack.h"

#include "macro.h"
#include "transgen.h"
#include "transgen-parser.h"
#include "transgen_f.h"

int line_no = 1;


%}

ws	[ \t]+
real 	[-\+]?[0-9]+\.[0-9]*
exp	[eE][-\+]?[0-9]+
integer [-\+]?[0-9]+
pointer 0x[0123456789abcdefABCDEF]+
qstring \"([^"]|\\["])*\" 
nameid  [A-Z_a-z]+[0-9A-Z_a-z\-]*
id    ([^ \t\n~0123456789:"&\(\)\$@;\|\.[\]][^ \t\n:"\(\)\]]*)
numberid	([^ \t\n"&\(\)\$@;\|\.\]][^ \t\n"\(\)\]]*)
idbar   (\|[^|]+\|)
nl	\n



%%
{ws}		;

module 		{return MODULE_TK;};
cflag 		{return CFLAG_TK;};
supervisor 	{return SUPERVISOR_TK;};

\{ 		{return OP_CPAR_TK;};
\} 		{return CL_CPAR_TK;};
\: 		{return COLON_TK;};
\; 		{return SEMICOLON_TK;};


\/\*  { char c; loop: 				/* Pompe chez Genom (MH et SF) */
         while ((c = input()) != '*')
             if (c == '\n') 
                 line_no++ ;
         switch (input()) {
            case '/': break;
            case '\n': line_no++; goto loop;
            case '*': unput('*');
            default: goto loop;
	  }
     };

{nameid}        { NEW_STR(transgen_yylval.string,transgen_yytext);
		  return SYMBOL_TK;};

{qstring}	{ NEW_QSTR(transgen_yylval.string,transgen_yytext);
		  return QSTRING_TK;};

\n      	line_no++;

.		{transgen_yyerror("unexpected character.");}


%%

int parse_transgen(FILE *file_stream)
{
     transgen_yy_switch_to_buffer(transgen_yy_create_buffer(file_stream,YY_BUF_SIZE));

     return transgen_yyparse();
}
