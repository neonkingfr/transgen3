static const char* const rcsid = "$Id$";
/*                               -*- Mode: C -*- 
 * transgen-main.c -- 
 * 
 * Copyright (C) 1999-2012 LAAS/CNRS
 * 
 * Author          : Francois Felix Ingrand
 * Created On      : Thu Jul  8 12:13:58 1999
 * Last Modified By: Francois Felix Ingrand
 * Last Modified On: Tue Sep 21 10:17:19 1999
 * Update Count    : 51
 * Status          : OK
 * 
 */

#include "config.h"

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/param.h>
#include <sys/stat.h>

#include "slistPack.h"
#include "slistPack_f.h"

#include "macro.h"
#include "transgen.h"
#include "transgen_f.h"
#include "perl-build_f.h"

char *transgen_dir=  ROOT_TRANSGEN;
char *canvas_dir=  ROOT_TRANSGEN;
FILE *perl_file;

Supervisor *supervisor = NULL;
Module *current_module = NULL;

int debug_flg = 0;

char *transgen_filename;

static char cwd[MAXPATHLEN];

int main(int argc, char **argv, char ** envp)
{
     int c;
     extern int optind;
     static const char *autoDir = "auto";
     static const char *userDir = "user";
     extern char *optarg;
     int filename_flg = 0, help_flg = 0, no_generation_flg = 0, keep_flg = 0;
     int fatal_error = 0, status;

     FILE *file_stream;
     struct stat statfile;
     char yesFlag;
     char perl_filename[MAXPATHLEN];
     char perl_cmd[MAXPATHLEN];

     while ((c = getopt(argc, argv, "f:hndk")) != EOF)
	  switch (c)
	  {
	  case 'f':
	       filename_flg++;
	       transgen_filename = optarg;
	       break;
	  case 'n':
	    no_generation_flg = 1;
	    break;
	  case 'k':
	    keep_flg = 1;
	    break;
	  case 'd':
	    debug_flg = 1;
	    break;
	  case 'h':
	  default:
	       help_flg++;
 
	  }
     if (help_flg) {
	  fprintf(stderr, "usage: transgen [-n] [-k] [-f] file\n");
	  exit(1);
     }

     if ( !filename_flg ){
	  if ( argc < (optind + 1)) {
	       fprintf(stderr, "ERROR: We need the name a file.\n");
	       exit (1);
	  } else
	       transgen_filename = argv[optind];
     }

     if (!(file_stream = fopen(transgen_filename,"r"))) {
          fprintf(stderr, "transgen ERROR: cannot open file %s (read mode)\n", 
		  transgen_filename);
	  perror("transgen: open file");
	  exit(1);
     }


    /* Get working dir */
    if (getcwd(cwd, MAXPATHLEN) == 0) {
	perror ("transgen: getcwd");
	exit(2);
    }

    if (parse_transgen(file_stream)) {
         /* failure */
         fprintf(stderr, "transgen : parse_transgen ERROR\n");
	 exit(1);
    }
    
     /*
     * Generation du fichier perl
     */

    /* En-tete du fichier */
    sprintf(perl_filename, "./%s.pl", supervisor->name);
    if ((perl_file = fopen(perl_filename, "w")) == NULL) {
        fprintf(stderr, "transgen : cannot open file %s (write mode)\n",
		perl_filename);
        perror(argv[0]);
        exit(2);
    }

    fprintf(perl_file, "#!/usr/local/bin/perl\n");
    fprintf(perl_file, "# Generateur de superviseur %s\n\n", supervisor->name);

    /* Variables generales pour perl */
    fprintf(perl_file, "$supervisor=\"%s\";\n", supervisor->name);

    fprintf(perl_file, "$canvasDir=\"%s\";\n", canvas_dir);

    /* Debut du fichier perl */
    script_do(perl_file, canvas_dir, "start.pl");

    /* Le supervisor */
    fatal_error = ! (make_transgen(perl_file)  &&
		     configure_transgen(perl_file)  &&
		     data_transgen(perl_file));

    /* On termine */
    script_do(perl_file, canvas_dir, "end.pl");
    fclose(perl_file);
 

    if (!fatal_error) {
	/* 
	 * Execution de perl 
	 */
	
	if (chmod(perl_filename, 0775) < 0) {
	    perror("chmod");
	    exit(1);
	}
	if (!no_generation_flg) {
	     sprintf(perl_cmd, "perl %s\n", perl_filename);
	     fprintf(stderr, "%s", perl_cmd);
	    status = system(perl_cmd);
	    if (!keep_flg ) unlink(perl_filename);
	} else {
	    fprintf(stderr, "File %s is written\n", perl_filename);
	    status = 0;
	}
    
	if (status == 0) {
	    if (!keep_flg ) unlink(perl_filename);
	     fprintf(stderr, "Done.\n");
	     exit(0);
	} else {
	    /* erreur durant l'execution du script */
	    perror ("transgen: Fatal error");
	    exit(2);
	}
	    
    } else {
	/* Erreur detecte'e pendant la generation */
	fprintf(stderr, "No perl code generated.\n");
	if (!keep_flg ) unlink(perl_filename);
	exit (2);
    }
}

Supervisor *make_supervisor(char *name, Slist *mods, char *cflags)
{
     Supervisor *supervisor= MAKE_OBJECT(Supervisor);
     

     if (cflags)
       NEW_STR(supervisor->cflag, cflags);
     NEW_STR(supervisor->name, name);
     NEW_STR(supervisor->NAME, name);
     UPPERCASE_STRING_IN_PLACE(supervisor->NAME);

     NEW_STR(supervisor->filename, transgen_filename);
     supervisor->mod_list = mods;
     NEW_STR(supervisor->dir, cwd);

     printf("Supervisor %s, %s\n", supervisor->name, supervisor->filename);
     return supervisor;
}

Module *make_module(char *name, char *path)
{
     static PBoolean message_printed = FALSE;
     Module *module = MAKE_OBJECT(Module);
     char pkg_config_cmd[MAXPATHLEN];

     
     
     if (path && ! message_printed) {
	  printf("The module path is now ignored (%s).\n\
One has to properly set PKG_CONFIG_PATH to get the proper module.\n\
	(you can reinvoke transgen with -d to see the include\n\
	and libs files which will be used.)\n", path);
	  message_printed = TRUE;
     }

     NEW_STR(module->name, name);
     NEW_STR(module->NAME, name);
     UPPERCASE_STRING_IN_PLACE(module->NAME);
     NEW_STR(module->NAME_SHV, name);
     UPPERCASE_STRING_IN_PLACE_RPL_MINUS(module->NAME_SHV);

/*      NEW_STR(module->path, path); */

     printf("Module %s.\n", name);
     if (debug_flg) {
       int ignore;
       sprintf(pkg_config_cmd, "pkg-config  --cflags --libs %s\n", name);
       ignore = system(pkg_config_cmd);
     }
     return module;
}

#include <stdio.h>
     
void transgen_yyerror(char *s)
{
     fprintf(stderr, "\"%s\", line %d: %s\n", "input", line_no, s);
}


