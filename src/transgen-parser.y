/*                               -*- Mode: C -*- 
 * transgen.y -- 
 * 
 * Copyright (C) 1999-2012 Francois Felix Ingrand, LAAS/CNRS.
 * 
 * Author          : Francois Felix Ingrand
 * Created On      : Mon Jun 28 22:04:46 1999
 * Last Modified By: Francois Felix Ingrand
 * Last Modified On: Sat Sep  4 15:18:59 1999
 * Update Count    : 41
 * Status          : OK
 */

%{
static const char* const rcsid = "$Id$";

#include "config.h"

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "slistPack.h"
#include "slistPack_f.h"

#include "macro.h"
#include "transgen.h"
#include "transgen_f.h"

%}

%union {
     char    		*string;    
     int    		cmd;        
     Slist 		*slist;
     void 		*module;
}

%token <cmd>	 	OP_CPAR_TK CL_CPAR_TK COLON_TK SEMICOLON_TK
%token <cmd>	 	MODULE_TK SUPERVISOR_TK CFLAG_TK
%token <string> 	SYMBOL_TK QSTRING_TK

%type <string> 		checked_path name opt_arg
%type <slist>		liste_module
%type <module>		module

%%

supervisor_file: supervisor
;

supervisor: SUPERVISOR_TK name OP_CPAR_TK liste_module opt_arg CL_CPAR_TK
              {supervisor = make_supervisor($2,$4,$5);}
          | SUPERVISOR_TK name OP_CPAR_TK liste_module CL_CPAR_TK
              {supervisor = make_supervisor($2,$4,NULL);}
;

liste_module: module					{$$=sl_make_slist(); sl_add_to_tail($$,$1);}
	| liste_module module				{$$=$1; sl_add_to_tail($1,$2);} 
;

module: MODULE_TK COLON_TK name checked_path SEMICOLON_TK	{$$ = make_module($3,$4);}
      | MODULE_TK COLON_TK name SEMICOLON_TK	                {$$ = make_module($3,NULL);}
;

checked_path: QSTRING_TK 				{$$=$1;}
;

opt_arg: CFLAG_TK COLON_TK QSTRING_TK SEMICOLON_TK	{$$=$3;}
;

name: SYMBOL_TK 				{$$=$1;}
;

%%

