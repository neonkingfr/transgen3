/*                               -*- Mode: C -*- 
 * transgen.h -- 
 * 
 * Copyright (C) 1999-2012 LAAS/CNRS
 * 
 * Author          : Francois Felix Ingrand
 * Created On      : Tue Jun 29 08:44:31 1999
 * Last Modified By: Francois Felix Ingrand
 * Last Modified On: Sat Sep  4 14:32:22 1999
 * Update Count    : 17
 * Status          : OK
 * 
 * $Id$
 */

#define TRUE 1
#define FALSE 0

typedef char PBoolean;

typedef struct module {
     char *name;
     char *NAME;
     char *NAME_SHV;
     char *path;
} Module;


typedef struct supervisor {
     char *name;
     char *NAME;
     char *filename;
     char *dir;			/* Which dir are we making the generation. */
     Slist *mod_list;
     char *cflag;
} Supervisor;


extern char *transgen_dir;
extern char *canvas_dir;
extern int line_no;
extern int debug_flg;
extern Supervisor *supervisor;
extern Module *current_module;
