void transgen_yyerror(char *s);
int transgen_yylex( void );
Module *make_module(char *name, char *path);
Supervisor *make_supervisor(char *name, Slist *mods, char *cflags);
int parse_transgen(FILE *file_stream);
int transgen_yyparse(void);
PBoolean configure_transgen(FILE *out);


extern PBoolean make_transgen(FILE *perl_file);
extern PBoolean data_transgen(FILE *perl_file);
extern PBoolean superv_transgen(FILE *perl_file);
